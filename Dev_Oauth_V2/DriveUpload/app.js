$(document).ready(function(){
     
    // client id
    let clientId = "43994306169-lvsr3h4k9750e2acmujkem3de67ciod4.apps.googleusercontent.com";

    // redirect_uri
    let redirectUri = "http://localhost:8080/DriveUpload/uploadFile.html";

    // set scope to access only the google drive api
    let scope = "https://www.googleapis.com/auth/drive";

    // url that user is redirected to
    let url = "";

    //event click listener for 'upload' button
    $("#signin").click(function(){
       LogIn(clientId,redirectUri,scope,url);
    });

    function LogIn(clientId,redirectUri,scope,url){
     
       // the url which the end user is redirected to
       url = "https://accounts.google.com/o/oauth2/v2/auth?redirect_uri="+redirectUri
       +"&prompt=consent&response_type=code&client_id="+clientId+"&scope="+scope
       +"&access_type=offline";

       //Redirects the user to the defined url
       window.location = url;
    }

});