$(document).ready(function(){
    
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get('code');
    
    // redirect_uri
    const redirectUri = "https://localhost:8080/DriveUpload/uploadFile.html"
    
    // set scope to access only the google drive api
    const scope = "https://www.googleapis.com/auth/drive";
    
    // client id
    let clientId = "43994306169-lvsr3h4k9750e2acmujkem3de67ciod4.apps.googleusercontent.com"// replace it with your client id;
    // client secret
    const clientSecret = "IPwRol3RFCxTVKUR1Ybt0r7H";    
    
    let access_token= "";//DeleteThis

    $.ajax({
        type: 'POST',
        url: "https://www.googleapis.com/oauth2/v4/token",
        data: 
        {   
            code:code,
            redirect_uri:redirectUri,
            client_secret:clientSecret,
            client_id:clientId,
            scope:scope,
            grant_type:"authorization_code"
        },
        dataType: "json",
        success: function(resultData) {

           //After sucessfully getting the authorization tokens store them in the local storage
           
           localStorage.setItem("accessToken",resultData.access_token);
           localStorage.setItem("refreshToken",resultData.refreshToken);
           localStorage.setItem("expires_in",resultData.expires_in);

           window.history.pushState({}, document.title, "/GoogleFileUploadApp/" + "uploadFile.html");
           
        }
    });

    function stripQueryStringAndHashFromPath(url) {
        return url.split("?")[0].split("#")[0];
    }   //DeleteThis

    //Set the file to be uploaded
    let Upload = function (fileToUpload) {
        this.file = fileToUpload;
    };
    
    //Sets the file details

    Upload.prototype.getName = function() {
        return this.file.name;
    };

    Upload.prototype.getType = function() {
        localStorage.setItem("type",this.file.type);
        return this.file.type;
    };

    Upload.prototype.getSize = function() {
        localStorage.setItem("size",this.file.size);
        return this.file.size;
    };

    //Do the file upload
    Upload.prototype.executeUpload = function () {
        
        var that = this;
        var formData = new FormData();
    
        // add assoc key values, this will be posts values
        formData.append("file", this.file, this.getName());
        formData.append("upload_file", true);
    
        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                //Adds the autherization token to request header
                request.setRequestHeader("Authorization", "Bearer" + " " + localStorage.getItem("accessToken"));
                
            },
            url: "https://www.googleapis.com/upload/drive/v2/files",
            data:{
                uploadType:"media"
            },
            xhr: function () {
                // Xhr-XMLHttpRequest
                var Xhr = $.ajaxSettings.xhr();
                if (Xhr.upload) {
                    // Add an event listner to XMLHttpRequest to track the progress
                    Xhr.upload.addEventListener('progress', that.progressHandling, false);
                }
                return Xhr;
            },
            success: function (data) {
                console.log(data);
            },
            error: function (error) {
                console.log(error);
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    };
    
    Upload.prototype.progressHandling = function (event) {
        var percent = 0;
        var position = event.loaded || event.position;
        var total = event.total;
        var progress_bar_id = "#progress-wrp";
        if (event.lengthComputable) {
            percent = Math.ceil(position / total * 100);
        }
        // update progressbars classes so it fits your code
        $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
        $(progress_bar_id + " .status").text(percent + "%");
    };

    $("#upload").on("click", function (e) {
        let fileToUpload = $("#files")[0].files[0];
        let uploadFile = new Upload(fileToUpload);
        // Proceed upload
        uploadFile.executeUpload();
    });

});